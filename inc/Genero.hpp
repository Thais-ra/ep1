#ifndef GENERO_HPP
#define GENERO_HPP
#include <iostream>
#include <string>
#include "Filme.hpp"
#include "Menu.hpp"

using namespace std;

class Genero: public Filme{
    private:

        string aventura;
        string comedia;
        string drama;
        string romance;
        string terror;
        int comando;

    public:

        Genero();
        Genero(string codigo, string titulo);
        ~Genero();

        void Escolhe_genero(Filme *filme);
        void Procura();
        void Cadastro_aventura(Filme *filme);
        void Cadastro_comedia(Filme *filme);
        void Cadastro_drama(Filme *filme);
        void Cadastro_romance(Filme *filme);
        void Cadastro_terror(Filme *filme);
};
#endif