#ifndef FILME_HPP
#define FILME_HPP

#include <string>

using namespace std;

class Filme{

	protected:
		string codigo;
		string titulo;
		int ano;
		int estoque;
		int quantidade;
		float valor;

	public:
		Filme();
		Filme(string titulo, float valor);
		~Filme();

		void set_codigo(string codigo);
		string get_codigo();
		
		void set_titulo(string titulo);
		string get_titulo();

		void set_ano(int ano);
		int get_ano();
		
		void set_estoque(int estoque);
		int get_estoque();

		void set_quantidade(int quantidade);
		int get_quantidade();

		void set_valor(float valor);
		float get_valor();

		void Imprime_dados();
		void Cadastro();
		static void Mostra();
		void Alugar();
		void Alugar_socio();
		void Devolver();
};

#endif
