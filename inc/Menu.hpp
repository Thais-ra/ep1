#ifndef MENU_HPP
#define MENU_HPP


class Menu{
    private:
        int comando;

    public:
        Menu();
        ~Menu();

        void set_comando(int comando);
	    int get_comando();
    
        void Principal();
        void Letreiro();
        void Estoque();
        bool Navegacao();
        void Finaliza_principal();
        void Finaliza_estoque();
        void Escolhe_genero();

};
#endif