#ifndef SOCIO_HPP
#define SOCIO_HPP

#include "Cliente.hpp"

using namespace std;

class Socio: public Cliente{
    private:
        long int CEP;

    public:
        Socio();
        ~Socio();

        void set_CEP(long int CEP);
        int get_CEP();

        static bool Confere_cadastro(string CPF);
        void Cadastro(string CPF);
};

#endif