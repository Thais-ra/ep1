#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <string>

using namespace std;

class Cliente{

	protected:
		string CPF;
		string nome;
		int ano_nasc;
		long int telefone;
		string email;
	
	public:
		Cliente();
		~Cliente();

		void set_CPF(string CPF);
		string get_CPF();

		void set_nome(string nome);
		string get_nome();

		void set_ano_nasc(int ano_nasc);
		int get_ano_nasc();

		void set_telefone(long int telefone);
		long int get_telefone();

		void set_email(string email);
		string get_email();

		static bool Confere_cadastro(string CPF);
		void Cadastro(string CPF);
		
};

#endif