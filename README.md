# EP1 - OO 2019.2 (UnB - Gama)

## Descrição

Estamos em 1995 e Victoria possui uma locadora de filmes que está fazendo muito sucesso em seu pequeno bairro. Por conta da grande demanda, ela conseguiu reduzir significativamente o preço dos alugueis de VHS.

Apesar do sucesso, Victoria pensa alto e quer alavancar sua locadora. Em uma noite de inspiração, Victoria pensou em duas estratégias para atrair mais pessoas:
- Oferecer descontos de 15% para clientes sócios;
- Oferecer filmes recomendados exclusivamente para cada cliente;

Para colocar as ideias em prática, ela deve abandonar seu velho hábito de utilizar seu estimado caderninho para gerenciar a locadora. 
Esse *software* foi desenvolvido para ajudar Victoria a gerenciar sua locadora e implementar as novas estratégias de negócio. 

### Menu Principal

Possui 5 funcionalidades:

- Alugar: Aqui Victoria poderá realizar as operações de aluguel da locadora, sendo que tais operações só poderão ser realizadas a partir da identificação do cliente, que ocorrerá por meio do CPF;
- Devolver: Victoria poderá devolver filmes alugados para o estoque;
- Cadastrar novo cliente: Cadastro de novos clientes, podendo eles ser sócios ou não;
- Recomendações: Espaço em manutenção, será implementado após um segundo contrato;
- Estoque: Aqui temos um novo menu para manipular funções relacionadas a entrada de novos filmes;
    - Cadastrar novos filmes: Cadastro de novos filmes, tendo que ser associado a um gênero;
    - Adicionar gênero a filmes já cadastrados: Aqui Victoria poderá adicionar novos gêneros a filmes anteriormente cadastrados;
    - Ver todos os filmes cadastrados: Aqui Victoria terá acesso a todos os filmes já cadastrados na locadora e poderá ver facilmente quanto cada um possui em estoque;

## Orientações

Para rodar o programa é necessario acessar a pasta da ep1 pelo terminal, compilar usando o comando *make* e o executar usando o comando *make run*.

## Desenvolvimento

O sistema foi feito em C++, e para tornar o programa mais eficiente, todos os dados cadastrados são salvos em arquivos. Assim, mesmo que se encerre a execução do programa os dados continuarão disponíveis.

Para o desenvolvimento do programa foram criados 5 classes, sendo elas:

- Cliente: Responsável pelo cadastro de clientes e confirmação se o cadastro foi ou não realizado.
- Socio: Classe herdeira de cliente, possui funções bastante semelhantes, porem lida com os dados de clientes sócios.
- Filme: A classe possui métodos que lidam diretamente com manutenção dos produtos.
- Gênero: Classe herdeira de Filme, aqui temos métodos que lidam com o cadastro dos filmes em cada gênero pertencentea ele. A estrutura da classe foi pensada a organizar melhor o modo de recomendação.
- Menu: Classe responsável pela navegação entre funcionalidades.

Para auxiliar no desenvolvimento do *software*, foi aberto algumas issues no repositório. 
