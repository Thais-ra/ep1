#include <string>
#include <iostream>
#include "Menu.hpp"
#include "Cliente.hpp"
#include "Filme.hpp"
#include "Socio.hpp"
#include "Genero.hpp"

using namespace std;

Menu::Menu(){
    comando = 0;
}
Menu::~Menu(){

}

void Menu::Letreiro(){
    cout << "" << endl;
    cout << "------------------------------------------------------------------------------" << endl;
    cout << "--------  _     _____  ____     __      ____    _____  ____     __     -------" << endl;
    cout << "-------- | |   |  _  ||  __|   /  \\    |  _ \\  |  _  ||  _ \\   /  \\    -------" << endl;
    cout << "-------- | |   | | | || |     /    \\   | | \\ | | | | || |_| | /    \\   -------" << endl;
    cout << "-------- | |__ | |_| || |__  /  __  \\  | |_/ | | |_| ||    / /  __  \\  -------" << endl;
    cout << "-------- |____||_____||____|/__/  \\__\\ |____/  |_____||_|\\_\\/__/  \\__\\ -------" << endl;
    cout << "------------------------------------------------------------------------------" << endl;
    cout << "" << endl;
}

bool Menu::Navegacao(){
    cout << "Digite (1) para continuar e (0) para voltar" << endl;
    cin >> comando;
    if(comando == 1)
        return true;
    if(comando == 0)
        return false; 
    else
        Navegacao();
}

void Menu::Finaliza_principal(){
    cout << "Digite (0) para sair ou (1) para voltar" << endl;
    cin >> comando;
    if(comando == 1)
        Principal();
    if(comando == 0)
        exit(0);
}

void Menu::Finaliza_estoque(){
    cout << "Digite (0) para sair ou (1) para voltar" << endl;
    cin >> comando;
    if(comando == 1)
        Estoque();
    if(comando == 0)
        exit(0);
}

void Menu::Principal(){
    Letreiro();
    cout << "-------- M E N U --------" << endl;

        cout << "(1) Alugar" << endl;
        cout << "(2) Devolver" << endl;
        cout << "(3) Cadastrar novo cliente" << endl;
        cout << "(4) Recomendações" << endl;
        cout << "(5) Estoque" << endl;
        cout << "(0) Sair" << endl;
        cout << "----------------------------" << endl;
        cout << "Digite a opção desejada: ";
        cin >> comando;
        system("clear");

    switch(comando){
        case 0:
            exit(0);
        break;
        case 1:
        {
            string CPF;
            cout << "Digite seu CPF" << endl;
            cin >> CPF;
            if(Cliente::Confere_cadastro(CPF)){
                Filme *novo = new Filme();
                novo->Alugar();
            } 
            else if(Socio::Confere_cadastro(CPF)){
                Filme *novo = new Filme();
                novo->Alugar_socio();
            }
            else{
                cout << "O cliente não possui cadastro" << endl;
            }
            Finaliza_principal();
        }
        break;
        case 2:
        {
            Filme *novo = new Filme();
            novo->Devolver();
            Finaliza_principal();
        }
        break;
        case 3:
        {   
            string CPF;
            cout << "Digite o CPF: " << endl;
	        cin >> CPF;
            if(Cliente::Confere_cadastro(CPF) || Socio::Confere_cadastro(CPF)){
                cout << "O cliente já possui cadastro!" << endl;
                Principal();
            } else{
                cout << "O cliente será associado à loja?" << endl;
                cout << "(1) Sim" << endl;
                cout << "(0) Não" << endl;
                cin >> comando;
                if(comando == 1){
                    Socio *novo = new Socio();
                    novo->Cadastro(CPF);
                } else if(comando == 0) {
                    Cliente *novo = new Cliente();
                    novo->Cadastro(CPF);
                }
                Principal();
            }
        }
        break;
        case 4:
        {
            cout << "Estamos em manutenção" << endl;
            Finaliza_principal();
        }
        break;
        case 5:
            Estoque();
        break;
            
        default:
            cout << "Opção inválida" << endl;
    }

}

void Menu::Estoque(){
    Letreiro();
    cout << "-------- E S T O Q U E --------" << endl;

        cout << "(1) Cadastrar novos filmes" << endl;
        cout << "(2) Adicionar gênero a filmes já cadastrados" << endl;
        cout << "(3) Ver todos os filmes cadastrados" << endl;
        cout << "(0) Voltar" << endl;
        cout << "----------------------------" << endl;
        cout << "Digite a opção desejada: ";
        cin >> comando;
        system("clear");

        switch(comando){
                
            case 0:
                Principal();
            break;
            case 1:
                {
                    cout << "Confira se o seu título já foi cadastrado anteriormente..." << endl;
	                cout << "" << endl;
                    Filme::Mostra();
                    if(Menu::Navegacao()){
                        Filme *novo = new Filme();
                        novo->Cadastro();
                        Genero *genero = new Genero();
                        genero->Escolhe_genero(novo);
                        Finaliza_estoque();
                    }
                }
            break;
            case 2:
                {
                    Filme::Mostra();
                    Genero *novo = new Genero();
                    novo->Procura();
                    Finaliza_estoque();
                }
            break;
            case 3:
                {
                    Filme::Mostra();
                    Finaliza_estoque();
                }
            break;
            default:
                cout << "Opção inválida" << endl;
        }
}