#include <iostream>
#include <fstream>
#include <vector>
#include "Cliente.hpp"

Cliente::Cliente(){
	CPF = "";
	nome = "";
	ano_nasc = 0;
	telefone = 0;
	email = "";
}
Cliente::~Cliente(){}

void Cliente::set_CPF(string CPF){
	this->CPF = CPF;
}
string Cliente::get_CPF(){
	return CPF;
}

void Cliente::set_nome(string nome){
	this->nome = nome;
}
string Cliente::get_nome(){
	return nome;
}

void Cliente::set_ano_nasc(int ano_nasc){
	this->ano_nasc = ano_nasc;
}
int Cliente::get_ano_nasc(){
	return ano_nasc;
}

void Cliente::set_telefone(long int telefone){
	this->telefone = telefone;
}
long int Cliente::get_telefone(){
	return telefone;
}

void Cliente::set_email(string email){
	this->email = email;
}
string Cliente::get_email(){
	return email;
}

bool Cliente::Confere_cadastro(string CPF){

	ifstream arquivo;
	string linha;

	arquivo.open("./doc/Clientes.txt");

	if(arquivo.is_open()){
    	while(getline(arquivo, linha)){
			if(linha == CPF){
				arquivo.close();
				return true;
			}
		}
  	} else 
      	cout << "Não foi possível abrir o arquivo" << endl;
	return false;
}

void Cliente::Cadastro(string CPF){

	fstream arquivo;
	arquivo.open("./doc/Clientes.txt", ios::out | ios::app);

	Cliente cliente1;

	if(arquivo.is_open()){
		cliente1.set_CPF(CPF);
		arquivo << cliente1.get_CPF() << endl;

		cout << "Nome: " << endl;
		cin.ignore();
		getline(cin, nome);
		cliente1.set_nome(nome);
	
		cout << "Ano de nascimento: " << endl;
		cin >> ano_nasc;
		cliente1.set_ano_nasc(ano_nasc);
		
		cout << "Telefone: " << endl;
		cin >> telefone;
		cliente1.set_telefone(telefone);

		cout << "E-mail" << endl;
		cin >> email;
		cliente1.set_email(email);

		arquivo << cliente1.get_nome() << endl;
		arquivo << cliente1.get_ano_nasc() << endl;
		arquivo << cliente1.get_telefone() << endl;
		arquivo << cliente1.get_email() << endl;
		arquivo << "----------------------------------" << endl;		
		arquivo.close();
		system("clear");
		cout << "Cadastro realizado com sucesso!!" << endl;
	} else
    	cout << "Não foi possível abrir o arquivo" << endl;
}