#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include "Filme.hpp"
#include "Menu.hpp"

Filme::Filme(){

	codigo = "";
	titulo = "";
	ano = 0;
	estoque = 0;
	quantidade = 0;
	valor = 15.0f;
}
Filme::Filme(string titulo, float valor){
	set_titulo(titulo);
	set_valor(valor);
}
Filme::~Filme(){

}

void Filme::set_codigo(string codigo){
	this->codigo = codigo;
}
string Filme::get_codigo(){
	return codigo;
}

void Filme::set_titulo(string titulo){
	this->titulo = titulo;
}
string Filme::get_titulo(){
	return titulo;
}

void Filme::set_ano(int ano){
	this->ano = ano;
}
int Filme::get_ano(){
	return ano;
}

void Filme::set_estoque(int estoque){
	this->estoque = estoque;
}
int Filme::get_estoque(){
	return estoque;
}

void Filme::set_quantidade(int quantidade){
	this->quantidade = quantidade;
}
int Filme::get_quantidade(){
	return quantidade;
}

void Filme::set_valor(float valor){
	this->valor = valor;
}
float Filme::get_valor(){
	return valor;
}

void Filme::Imprime_dados(){
	cout << get_titulo() << endl;
    cout << "Valor: R$ " << get_valor() << endl;

}

void Filme::Mostra(){
	fstream arquivo;
	string linha;
	cout << "------- Lista de filmes ------- " << endl;
	cout << "" << endl;
	arquivo.open("./doc/Filmes.txt", ios::in);
	if(arquivo.is_open()){
    	while(getline(arquivo, linha)){
			cout << linha << endl;	
		}
    	arquivo.close();
		cout << "" << endl;
  	} else 
    	cout << "Não foi possível abrir o arquivo" << endl;

}

void Filme::Cadastro(){
	cout << "------- Cadastro de Filmes -------" << endl;
	cout << "Código: " << endl;
	cin.ignore();
	cin >> codigo;

	ofstream arquivo;
	ifstream saida;
	string linha;

	saida.open("./doc/Filmes.txt");
	if(saida.is_open()){
		while(getline(saida, linha)){
			if(codigo == linha){
				cout << "Código já cadastrado! Tente novamente" << endl;
				Menu *volta = new Menu();
				volta->Finaliza_estoque();
			}
		}
	} else
		cout << "Não foi possível abrir o arquivo" << endl;


	arquivo.open("./doc/Filmes.txt", ios::app);

	if(arquivo.is_open()){
		cout << "Título: " << endl;
		cin.ignore();
		getline(cin, titulo);

		cout << "Ano: " << endl;
		cin >> ano;

		cout << "Quantidade em estoque: " << endl;
		cin.ignore();
		cin >> estoque;
	

		arquivo << get_codigo() << endl;
		arquivo << "Título: " << get_titulo() << endl;
		arquivo << "Ano: " << get_ano() << endl;
		arquivo << "Estoque: " << get_estoque() << endl;
		arquivo << "----------------------------------" << endl;		
		arquivo.close();
	} else
    	cout << "Não foi possível abrir o arquivo" << endl;
}

void Filme::Alugar(){
	cout << "Quantos filmes serão alugados? " << endl;
	cin >> quantidade;

	Mostra();
	vector<Filme*> alugar;

	fstream arquivo;
	string linha;
	int comando;
	int contador = 0;
	
	cout << "Digite o(s) código do(s) filme(s) que deseja alugar: " << endl;

	for(int i=0; i < quantidade; i++){
		cin >> codigo;

		vector<string> novo;
		stringstream converte;

		arquivo.open("./doc/Filmes.txt", ios::in);
		if(arquivo.is_open()){
			while(getline(arquivo, linha)){
				if(linha == codigo){
					contador++;
				}
				if(contador == 2){
					alugar.push_back(new Filme(linha, valor));
				}
				if(contador != 0){
					contador++;
				}
				if(contador == 5){
					contador = 0;
					cout << linha[9] << endl;
					converte << linha[9];
					int k;
					converte >> k;
					if(k > 0){
						k = k-1;
						novo.push_back("Estoque: " + to_string(k));
					}
					else{
						cout << "Não possuimos esse Filme em estoque" << endl;
						Menu *volta = new Menu();
						volta->Finaliza_principal();
					}
				}
				else{
					novo.push_back(linha);
				}	
			}
    		arquivo.close();
  		} else 
    		cout << "Não foi possível abrir o arquivo" << endl;
		arquivo.open("./doc/Filmes.txt", ios::out);
		if (arquivo.is_open()){
			for(int g = 0; g < novo.size(); g++){
				arquivo << novo[g];
				arquivo << endl;
			}
			arquivo.close();
		} else {
			cout << "Não foi possível abrir o arquivo" << endl;
		}
	}

	cout << "------ Dados de compras -----" << endl;
	for (Filme *f: alugar){
    	f->Imprime_dados();
    	cout << endl;
  	}
	cout << "Quantidade total de filmes alugados: " << quantidade << endl;
	cout << "Valor total: R$ " << quantidade*valor << endl;

}

void Filme::Alugar_socio(){
	cout << "Quantos filmes serão alugados? " << endl;
	cin >> quantidade;

	Mostra();
	vector<Filme*> alugar;
	
	fstream arquivo;
	string linha;
	int comando;
	int contador = 0;

	cout << "Digite o(s) código do(s) filme(s) que deseja alugar: " << endl;

	for(int i=0; i < quantidade; i++){
		cin >> codigo;

		vector<string> novo;
		stringstream converte;
		
		arquivo.open("./doc/Filmes.txt", ios::in);
		if(arquivo.is_open()){
			while(getline(arquivo, linha)){
				if(linha == codigo){
					contador++;
				}
				if(contador == 2){
					alugar.push_back(new Filme(linha, valor));
				}
				if(contador != 0){
					contador++;
				}
				if(contador == 5){
					contador = 0;
					converte << linha[9];
					int k;
					converte >> k;
					if(k > 0){
						k = k-1;
						novo.push_back("Estoque: " + to_string(k));
					}
					else{
						cout << "Não possuimos esse Filme em estoque" << endl;
						Menu *volta = new Menu();
						volta->Finaliza_principal();
					}
				}
				else{
					novo.push_back(linha);
				}	
			}
    		arquivo.close();
  		} else 
    		cout << "Não foi possível abrir o arquivo" << endl;
		arquivo.open("./doc/Filmes.txt", ios::out);
		if (arquivo.is_open()){
			for(int g = 0; g < novo.size(); g++){
				arquivo << novo[g];
				arquivo << endl;
			}
			arquivo.close();
		} else {
			cout << "Não foi possível abrir o arquivo" << endl;
		}
	}

	cout << "------ Dados de compras -----" << endl;
	for (Filme *f: alugar){
    	f->Imprime_dados();
    	cout << endl;
  	}
	float total;
	total = quantidade*valor;
	cout << "Quantidade total de filmes alugados: " << quantidade << endl;
	cout << "Valor total: R$ " << total << endl;
	total = total - (total*0.15);
	cout << "Valor com desconto: R$" << total << endl;
}

void Filme::Devolver(){
	cout << "Quantos filmes serão devolvidos? " << endl;
	cin >> quantidade;

	Mostra();
	vector<Filme*> devolver;

	fstream arquivo;
	string linha;
	int comando;
	int contador = 0;
	
	cout << "Digite o(s) código do(s) filme(s) que deseja devolver ao estoque: " << endl;

	for(int i=0; i < quantidade; i++){
		cin >> codigo;

		vector<string> novo;
		stringstream converte;

		arquivo.open("./doc/Filmes.txt", ios::in);
		if(arquivo.is_open()){
			while(getline(arquivo, linha)){
				if(linha == codigo){
					contador++;
				}
				if(contador == 2){
					devolver.push_back(new Filme(linha, valor));
				}
				if(contador != 0){
					contador++;
				}
				if(contador == 5){
					contador = 0;
					converte << linha[9];
					int k;
					converte >> k;
					k = k+1;
					novo.push_back("Estoque: " + to_string(k));
				}
				else{
					novo.push_back(linha);
				}	
			}
    		arquivo.close();
  		} else 
    		cout << "Não foi possível abrir o arquivo" << endl;
		arquivo.open("./doc/Filmes.txt", ios::out);
		if (arquivo.is_open()){
			for(int g = 0; g < novo.size(); g++){
				arquivo << novo[g];
				arquivo << endl;
			}
			arquivo.close();
		} else {
			cout << "Não foi possível abrir o arquivo" << endl;
		}
	}
	cout << "Filme devolvido com sucesso!" << endl;
}