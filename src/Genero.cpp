#include "Genero.hpp"
#include <fstream>
#include <vector>

Genero::Genero(){
    aventura = "";
    comedia = "";
    drama = "";
    romance = "";
    terror = "";
    comando = 0;
}
Genero::Genero(string codigo, string titulo){
	set_codigo(codigo);
	set_titulo(titulo);
}
Genero::~Genero(){

}

void Genero::Procura(){
	string codigo;
	string linha;
	int contador = 0;

	cout << "Digite o código do filme que pretende adicionar gênero: "; 
	cin >> codigo;
	
	vector<Filme*> novo;

	set_codigo(codigo);
	
	fstream arquivo;
	arquivo.open("./doc/Filmes.txt", ios::in);

	if (arquivo.is_open()){
    	while(getline(arquivo, linha)){
      		if(linha == codigo){
        		contador++;
    		}
	  		else if(contador == 1){
				set_titulo(linha);
				contador++;
	  		}
	    }
		novo.push_back(new Genero(get_codigo(), get_titulo()));
    	arquivo.close();
    } else {
    	cout << "Não foi possível abrir o arquivo" << endl;
		}
	Escolhe_genero(novo.back());
}

void Genero::Escolhe_genero(Filme *novo){

    cout << "(1) Aventura" << endl;
    cout << "(2) Comédia" << endl;
    cout << "(3) Drama" << endl;
    cout << "(4) Romance" << endl;
    cout << "(5) Terror" << endl;
    cout << "" << endl;
    cout << "O Filme pertence a qual gênero? " << endl;
    cin >> comando;

    switch(comando){
        case 1:
            Cadastro_aventura(novo);
        break;
        case 2:
            Cadastro_comedia(novo);
        break;
        case 3:
            Cadastro_drama(novo);
        break;
        case 4:
            Cadastro_romance(novo);
        break;
        case 5:
            Cadastro_terror(novo);
        break;
    }
}

void Genero::Cadastro_aventura(Filme *novo){
    ifstream leitura;
	ofstream escrita;
	leitura.open("./doc/Generos/Aventura.txt");

	if(leitura.is_open()){
		string linha;
		while(getline(leitura, linha)){
			if(linha == novo->get_codigo()){
				cout << "O filme já foi adicionado a esse gênero!" << endl;
				leitura.close();
				Menu *volta = new Menu();
                    volta->Principal();
			}
		}
	}
	else{
		cout << "Não foi possível abrir o arquivo" << endl;
	}
	escrita.open("./doc/Generos/Aventura.txt", ios::app);
    if(escrita.is_open()){
		escrita << novo->get_codigo() << endl;
		escrita << novo->get_titulo() << endl;
		escrita << "----------------------------------" << endl;		
		escrita.close();
		cout << "Filme cadastrado com sucesso" << endl;
	} else
    	cout << "Não foi possível abrir o arquivo" << endl;
}

void Genero::Cadastro_comedia(Filme *novo){
    ifstream leitura;
	ofstream escrita;
	leitura.open("./doc/Generos/Comedia.txt");

	if(leitura.is_open()){
		string linha;
		while(getline(leitura, linha)){
			if(linha == novo->get_codigo()){
				cout << "O filme já foi adicionado a esse gênero!" << endl;
				leitura.close();
				Menu *volta = new Menu();
                    volta->Principal();
			}
		}
	}
	else{
		cout << "Não foi possível abrir o arquivo" << endl;
	}
	escrita.open("./doc/Generos/Comedia.txt", ios::app);
    if(escrita.is_open()){
		escrita << novo->get_codigo() << endl;
		escrita << novo->get_titulo() << endl;
		escrita << "----------------------------------" << endl;		
		escrita.close();
		cout << "Filme cadastrado com sucesso" << endl;
	} else
    	cout << "Não foi possível abrir o arquivo" << endl;
}

void Genero::Cadastro_drama(Filme *novo){
    ifstream leitura;
	ofstream escrita;
	leitura.open("./doc/Generos/Drama.txt");

	if(leitura.is_open()){
		string linha;
		while(getline(leitura, linha)){
			if(linha == novo->get_codigo()){
				cout << "O filme já foi adicionado a esse gênero!" << endl;
				leitura.close();
				Menu *volta = new Menu();
                    volta->Principal();
			}
		}
	}
	else{
		cout << "Não foi possível abrir o arquivo" << endl;
	}
	escrita.open("./doc/Generos/Drama.txt", ios::app);
    if(escrita.is_open()){
		escrita << novo->get_codigo() << endl;
		escrita << novo->get_titulo() << endl;
		escrita << "----------------------------------" << endl;		
		escrita.close();
		cout << "Filme cadastrado com sucesso" << endl;
	} else
    	cout << "Não foi possível abrir o arquivo" << endl;
}

void Genero::Cadastro_romance(Filme *novo){
	    ifstream leitura;
	ofstream escrita;
	leitura.open("./doc/Generos/Romance.txt");

	if(leitura.is_open()){
		string linha;
		while(getline(leitura, linha)){
			if(linha == novo->get_codigo()){
				cout << "O filme já foi adicionado a esse gênero!" << endl;
				leitura.close();
				Menu *volta = new Menu();
                    volta->Principal();
			}
		}
	}
	else{
		cout << "Não foi possível abrir o arquivo" << endl;
	}
	escrita.open("./doc/Generos/Romance.txt", ios::app);
    if(escrita.is_open()){
		escrita << novo->get_codigo() << endl;
		escrita << novo->get_titulo() << endl;
		escrita << "----------------------------------" << endl;		
		escrita.close();
		cout << "Filme cadastrado com sucesso" << endl;
	} else
    	cout << "Não foi possível abrir o arquivo" << endl;
}

void Genero::Cadastro_terror(Filme *novo){
    ifstream leitura;
	ofstream escrita;
	leitura.open("./doc/Generos/Terror.txt");

	if(leitura.is_open()){
		string linha;
		while(getline(leitura, linha)){
			if(linha == novo->get_codigo()){
				cout << "O filme já foi adicionado a esse gênero!" << endl;
				leitura.close();
				Menu *volta = new Menu();
                    volta->Principal();
			}
		}
	}
	else{
		cout << "Não foi possível abrir o arquivo" << endl;
	}
	escrita.open("./doc/Generos/Terror.txt", ios::app);
    if(escrita.is_open()){
		escrita << novo->get_codigo() << endl;
		escrita << novo->get_titulo() << endl;
		escrita << "----------------------------------" << endl;		
		escrita.close();
		cout << "Filme cadastrado com sucesso" << endl;
	} else
    	cout << "Não foi possível abrir o arquivo" << endl;
}