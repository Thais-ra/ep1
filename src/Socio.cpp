#include <iostream>
#include <vector>
#include <fstream>
#include "Socio.hpp"

using namespace std;

Socio::Socio(){
    set_CPF("");
	set_nome("");
	set_ano_nasc(0);
	set_telefone(0);
	set_email("");
    CEP = 0;
}
Socio::~Socio(){
}

void Socio::set_CEP(long int CEP){
    this->CEP = CEP;
}
int Socio::get_CEP(){
    return CEP;
}

bool Socio::Confere_cadastro(string CPF){

	ifstream arquivo;
	string linha;

	arquivo.open("./doc/Socios.txt");

	if(arquivo.is_open()){
    	while(getline(arquivo, linha)){
			if(linha == CPF){
				arquivo.close();
				return true;
			}
		}
  	} else 
      	cout << "Não foi possível abrir o arquivo" << endl;
	return false;
}

void Socio::Cadastro(string CPF){

	fstream arquivo;
	arquivo.open("./doc/Socios.txt", ios::out | ios::app);

	Socio *socio1 = new Socio();

	if(arquivo.is_open()){

		socio1->set_CPF(CPF);
		arquivo << socio1->get_CPF() << endl;

		cout << "Nome: " << endl;
		cin.ignore();
		getline(cin, nome);
		socio1->set_nome(nome);
	
		cout << "Ano de nascimento: " << endl;
		cin >> ano_nasc;
		socio1->set_ano_nasc(ano_nasc);
		
		cout << "Telefone: " << endl;
		cin >> telefone;
		socio1->set_telefone(telefone);

		cout << "E-mail" << endl;
		cin >> email;
		socio1->set_email(email);

        cout << "CEP" << endl;
		cin >> CEP;
		socio1->set_CEP(CEP);

		arquivo << socio1->get_nome() << endl;
		arquivo << socio1->get_ano_nasc() << endl;
		arquivo << socio1->get_telefone() << endl;
		arquivo << socio1->get_email() << endl;
        arquivo << socio1->get_CEP() << endl;
		arquivo << "----------------------------------" << endl;		
		arquivo.close();
		system("clear");
		cout << "Cadastro realizado com sucesso!!" << endl;
	} else
    	cout << "Não foi possível abrir o arquivo" << endl;
}